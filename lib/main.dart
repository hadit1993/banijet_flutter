import 'package:banijet_flutter/core/constants/colors.dart';
import 'package:banijet_flutter/core/cubit/cubit_observer.dart';
import 'package:banijet_flutter/feature/authentication/presentation/cubit/verify_auth_cubit.dart';
import 'package:banijet_flutter/feature/authentication/presentation/pages/request_auth_page.dart';
import 'package:banijet_flutter/feature/authentication/presentation/pages/splash.dart';
import 'package:banijet_flutter/feature/authentication/presentation/pages/verify_auth_page.dart';
import 'package:bloc/bloc.dart';

import 'package:flutter/material.dart';
import 'package:banijet_flutter/core/utils/injection_container.dart' as di;
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'core/cubit/input_validation_cubit.dart';
import 'core/utils/injection_container.dart';
import 'core/utils/input_validator.dart';
import 'core/utils/slider_left_route.dart';
import 'feature/authentication/presentation/cubit/request_auth_cubit.dart';
import 'feature/authentication/presentation/cubit/timer_cubit.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  Bloc.observer = CubitObserver();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: BanijetColors.PRIMARY,
      statusBarBrightness: Brightness.light,
    ));

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'IranSanse',
        primaryColor: BanijetColors.PRIMARY,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Splash(),
      initialRoute: 'splash',
      onGenerateRoute: (RouteSettings setting) {
        return mapNamesToRoutes(setting);
      },
    );
  }
}

SlideLeftRoute mapNamesToRoutes(RouteSettings setting) {
  Widget destination;

  final requestAuthCubit = sl<RequestAuthCubit>();
  final verifyAuthCubit = sl<VerifyAuthCubit>();

  switch (setting.name) {
    case 'splash':
      destination = Splash();
      break;
    case 'auth/request':
      destination = MultiBlocProvider(providers: [
        BlocProvider(
            create: (_) => InputValidationCubit(
                inputType: InputType.Phone,
                inputValidator: sl<InputValidator>())),
        BlocProvider.value(value: requestAuthCubit)
      ], child: RequestAuthPage());

      break;

    case 'auth/verify':
      final Map<String, Object> bundle = setting.arguments;
      destination = MultiBlocProvider(providers: [
        BlocProvider(
            create: (_) => InputValidationCubit(
                inputType: InputType.VerificationCode,
                inputValidator: sl<InputValidator>())),
        BlocProvider.value(value: requestAuthCubit),
        BlocProvider.value(value: verifyAuthCubit),
        BlocProvider.value(value: sl<TimerCubit>()),
      ], child: VerifyAuthPage(phone: bundle['phone'], expTime: bundle['exp']));
      break;
    default:
      destination = null;
  }

  return SlideLeftRoute(page: destination);
}

Route createRoute(Widget widget) {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => widget,
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(0.0, 1.0);
      var end = Offset.zero;
      var curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}

class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({ WidgetBuilder builder, RouteSettings settings })
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      Widget child) {
    if (settings.name == 'splash')
      return child;
    // Fades between routes. (If you don't want any animation,
    // just return child.)
    return new FadeTransition(opacity: animation, child: child);
  }
}
