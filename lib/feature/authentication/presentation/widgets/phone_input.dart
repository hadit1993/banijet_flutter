import 'package:banijet_flutter/core/constants/colors.dart';
import 'package:banijet_flutter/core/constants/text_resources.dart';
import 'package:banijet_flutter/core/cubit/input_validation_cubit.dart';
import 'package:banijet_flutter/core/cubit/request_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PhoneInput extends StatelessWidget {
  final bool hasError;
  final bool isDeleteIconVisible;
  final Function onDelete;
  final Function(String text) onChanged;
  final bool enabled;
  final bool isInputValid;
  final String errorMessage;

  final TextEditingController phoneController;

  PhoneInput({
    Key key,
    @required this.onDelete,
    @required this.isDeleteIconVisible,
    @required this.phoneController,
    @required this.hasError,
    @required this.onChanged,
    @required this.enabled,
    @required this.isInputValid,
    @required this.errorMessage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          height: 51.5,
          margin: EdgeInsets.only(top: 60),
          decoration: BoxDecoration(
              border: Border.all(
                width: 1,
                color: hasError
                    ? BanijetColors.ERROR_COLOR
                    : BanijetColors.DISABLED_COLOR,


              ),
              borderRadius: BorderRadius.all(Radius.circular(12))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [

              if (isDeleteIconVisible)
                Padding(
                  padding: EdgeInsets.only(right: 10),
                  child: GestureDetector(
                    key: Key('delIcon'),
                    child: Image.asset(
                      'assets/icons/ic_del_phone.png',
                      width: 19,
                      height: 19,
                    ),
                    onTap: onDelete

                    ,
                  ),
                ),
              Expanded(

                  child: Container(
                height: 51.5,
                child: TextField(
                  key: Key('phoneTextField'),
                  onChanged: onChanged,
                  enabled: enabled,
                  maxLength: 11,
                  style: TextStyle(fontSize: 14),
                  keyboardType: TextInputType.phone,
                  controller: phoneController,
                  textAlign: TextAlign.right,
                  decoration: InputDecoration(
                      hintStyle: TextStyle(color: BanijetColors.DISABLED_COLOR),
                      hintText: TextResources.PHONE_HINT,
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      counterText: ''),
                ),

              )


              ),

              Padding(
                padding: EdgeInsets.only(left: 10),
                child: GestureDetector(
                  child: Image.asset(
                    'assets/icons/ic_profile.png',
                    key: Key('inputProfileIcon'),
                    width: 19.9,
                    height: 19.9,
                    color: isInputValid ? null : BanijetColors.DISABLED_COLOR,

                    // validatorState is ValidInput
                    //     ? null
                    //     : BanijetColors.DISABLED_COLOR,
                  ),
                ),
              )
            ],
            //                     CrossAxisAlignment.center,
          ),
        ),
        if (hasError)
          Container(
            child: Text(
              errorMessage,
              style: TextStyle(
                fontSize: 12,
                color: BanijetColors.ERROR_COLOR,
              ),
              key: Key('errorText'),
            ),
            margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.only(right: 5),
            alignment: Alignment.topRight,
          ),
      ],
    );
  }
}
