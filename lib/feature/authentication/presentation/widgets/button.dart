import 'package:banijet_flutter/core/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class Button extends StatelessWidget {
  final bool isLoading;
  final String title;
  final Color titleColor;
  final bool disabled;
  final Function onPress;
  final Color splashColor;
  final Color color;
  final Color disabledColor;

  Button(
      {Key key,
      @required this.title,
      @required this.onPress,
      this.isLoading = false,
      this.disabled = false,
      this.titleColor = Colors.white,
      this.splashColor = const Color(0x66ffffff),
      this.color = BanijetColors.PRIMARY,
      this.disabledColor = BanijetColors.DISABLED_COLOR})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;

    return Material(
      key: Key('material'),
      borderRadius: BorderRadius.circular(10),
      child: InkWell(
        key: Key('inkWell'),
        child: Container(
            width: width,
            height: 50,
            child: isLoading
                ? Lottie.asset('assets/jsons/submit_loading.json',
                    animate: true, height: 30, key: Key('buttonLoading'))
                : Text(
                    title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: titleColor),
                    key: Key('buttonTitle'),
                  ),
            alignment: Alignment.center),
        onTap: isLoading || disabled ? null : onPress,
        splashColor: splashColor,
      ),
      color: disabled ? disabledColor : color,
    );
  }
}
