import 'package:banijet_flutter/core/utils/injection_container.dart';
import 'package:banijet_flutter/feature/authentication/presentation/cubit/get_profile_cubit.dart';
import 'package:banijet_flutter/feature/authentication/presentation/cubit/request_auth_cubit.dart';
import 'package:banijet_flutter/feature/authentication/presentation/cubit/timer_cubit.dart';
import 'package:banijet_flutter/feature/authentication/presentation/cubit/verify_auth_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CubitProvider extends StatelessWidget {
  final Widget child;

  CubitProvider({Key key, @required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(providers: [
      BlocProvider<RequestAuthCubit>(create: (_) => sl<RequestAuthCubit>()),
      BlocProvider<VerifyAuthCubit>(create: (_) => sl<VerifyAuthCubit>()),
      BlocProvider<TimerCubit>(create: (_) => sl<TimerCubit>()),
      BlocProvider<GetProfileCubit>(create: (_) => sl<GetProfileCubit>()),
    ], child: child);
  }
}
