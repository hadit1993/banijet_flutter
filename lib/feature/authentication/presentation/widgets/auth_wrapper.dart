import 'package:flutter/material.dart';

class AuthWrapper extends StatelessWidget {

  final Widget child;

  AuthWrapper({Key key, this.child}): super(key: key);

  @override
  Widget build(BuildContext context) {
    final statusBarHeight = MediaQuery.of(context).padding.top;
    final bottomNavBarHeight = MediaQuery.of(context).padding.bottom;

    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Padding(
          padding: EdgeInsets.only(top: statusBarHeight),
          child: Stack(
            children: [
              Container(
                color: Theme.of(context).primaryColor,
                height: 250,
              ),
              Image.asset(
                'assets/images/logo_semitrans.png',
                width: 188,
                height: 197,
              ),
              Padding(
                padding: EdgeInsets.only(top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/images/title_logo.png',
                      width: 176.8,
                      height: 29.9,
                    ),
                  ],
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(top: 100),
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30))),
                    height:
                    height - (100 + statusBarHeight + bottomNavBarHeight),
                    child: child,
                  ))
            ],
          )),
    );
  }
}
