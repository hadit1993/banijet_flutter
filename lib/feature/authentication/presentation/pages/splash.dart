import 'package:banijet_flutter/core/cubit/input_validation_cubit.dart';
import 'package:banijet_flutter/core/presentation/widgets/stateful_wrapper.dart';
import 'package:banijet_flutter/core/utils/injection_container.dart';
import 'package:banijet_flutter/core/utils/input_validator.dart';
import 'package:banijet_flutter/core/utils/slider_left_route.dart';
import 'package:banijet_flutter/feature/authentication/presentation/cubit/request_auth_cubit.dart';
import 'package:banijet_flutter/feature/authentication/presentation/pages/request_auth_page.dart';
import 'package:banijet_flutter/feature/authentication/presentation/pages/verify_auth_page.dart';
import 'package:banijet_flutter/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StatefulWrapper(
        onInit: () async {
          Future.delayed(const Duration(milliseconds: 2000), () {
            // Navigator.pushReplacementNamed(context, 'auth/request');
            Navigator.pushReplacementNamed(context, 'auth/verify',
                arguments: {'phone': '093********', 'exp': 120});
          });
        },
        child: Scaffold(
          body: Container(
            child: Center(child: Text('Splash Screen')
                // child: Text('Splash Screen'),
                ),
          ),
        ));
  }
}
