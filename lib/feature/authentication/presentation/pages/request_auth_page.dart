import 'package:banijet_flutter/core/constants/colors.dart';
import 'package:banijet_flutter/core/constants/text_resources.dart';
import 'package:banijet_flutter/core/cubit/input_validation_cubit.dart';
import 'package:banijet_flutter/core/cubit/request_cubit.dart';
import 'package:banijet_flutter/feature/authentication/domain/usecases/request_authentication.dart';
import 'package:banijet_flutter/feature/authentication/presentation/cubit/request_auth_cubit.dart';
import 'package:banijet_flutter/feature/authentication/presentation/widgets/button.dart';
import 'package:banijet_flutter/feature/authentication/presentation/widgets/phone_input.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RequestAuthPage extends StatelessWidget {
  final phoneController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final statusBarHeight = MediaQuery.of(context).padding.top;
    final bottomNavBarHeight = MediaQuery.of(context).padding.bottom;

    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Padding(
          padding: EdgeInsets.only(top: statusBarHeight),
          child: Stack(
            children: [
              Container(
                color: Theme.of(context).primaryColor,
                height: 250,
              ),
              Image.asset(
                'assets/images/logo_semitrans.png',
                width: 188,
                height: 197,
              ),
              Padding(
                padding: EdgeInsets.only(top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/images/title_logo.png',
                      width: 176.8,
                      height: 29.9,
                    ),
                  ],
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(top: 100),
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30))),
                    height:
                        height - (100 + statusBarHeight + bottomNavBarHeight),
                    child: BlocConsumer<InputValidationCubit,
                        InputValidationState>(
                      listener: (_, validatorState) {
                        if (validatorState is ValidInput) {
                          FocusManager.instance.primaryFocus.unfocus();
                        }
                      },
                      builder: (validatorContext, validatorState) {
                        return BlocBuilder<RequestAuthCubit, RequestState>(
                            builder: (requestContext, requestState) {
                          return Column(
                            children: [
                              const Padding(
                                  padding: EdgeInsets.only(top: 50),
                                  child: Center(
                                    child: Text(
                                      TextResources.ENTER_PHONE,
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  )),
                              PhoneInput(
                                  key: Key('phoneInput'),
                                  onDelete: () {
                                    phoneController.clear();
                                    BlocProvider.of<InputValidationCubit>(
                                            validatorContext)
                                        .validate('');
                                  },
                                  isDeleteIconVisible:
                                      validatorState.input.isNotEmpty &&
                                          !(requestState is RequestLoading),
                                  phoneController: phoneController,
                                  hasError: requestState is RequestError,
                                  onChanged: (text) {
                                    BlocProvider.of<InputValidationCubit>(
                                            validatorContext)
                                        .validate(text);
                                  },
                                  enabled: !(requestState is RequestLoading),
                                  isInputValid: validatorState is ValidInput,
                                  errorMessage: requestState is RequestError
                                      ? requestState.message
                                      : ''),
                              Expanded(
                                  child: Container(
                                alignment: Alignment.bottomCenter,
                                padding: EdgeInsets.only(bottom: 15),
                                child: BlocListener<RequestAuthCubit,
                                    RequestState>(
                                  listener: (rContext, rState) {
                                    if (rState is RequestSuccess) {
                                      final int expTimer = rState.result;
                                      final bundle = {
                                        'phone': validatorState.input,
                                        'exp': expTimer
                                      };

                                      Navigator.pushNamed(
                                          context, 'auth/verify',
                                          arguments: bundle);
                                    }
                                  },
                                  child: Button(
                                    key: Key('continue'),
                                    title: TextResources.CONTINUE,
                                    onPress: () {
                                      // BlocProvider.of<RequestAuthCubit>(context)
                                      //     .call(AuthRequestParam(
                                      //         phoneNumber: validatorState.input));
                                      final bundle = {
                                        'phone': validatorState.input,
                                        'exp': 120
                                      };

                                      Navigator.pushNamed(
                                          context, 'auth/verify',
                                          arguments: bundle);
                                    },
                                    isLoading: requestState is RequestLoading,
                                    disabled: validatorState is InvalidInput,
                                  ),
                                ),
                              )),
                            ],
                          );
                        });
                      },
                    ),
                  ))
            ],
          )),
    );
  }
}
