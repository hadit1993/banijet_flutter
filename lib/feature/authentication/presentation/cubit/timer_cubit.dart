import 'dart:async';

import 'package:banijet_flutter/feature/authentication/util/ticker.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'timer_state.dart';

class TimerCubit extends Cubit<TimerState> {
  final Ticker ticker;

  TimerCubit({@required this.ticker}) : super(TimerInactive());

  StreamSubscription<int> _tickerSubscription;

  void startTimer(int duration) {
    emit(TimerActive(duration));
    _tickerSubscription?.cancel();
    _tickerSubscription = ticker.tick(ticks: duration).listen((du) {
      if (du > 0) {
        emit(TimerActive(du));
      } else {
        stopTimer();
      }
    });
  }

  void stopTimer() {
    emit(TimerInactive());
    _tickerSubscription?.cancel();
  }
}
