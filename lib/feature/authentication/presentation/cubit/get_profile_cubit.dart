import 'package:banijet_flutter/core/cubit/request_cubit.dart';
import 'package:banijet_flutter/core/error/failures.dart';
import 'package:banijet_flutter/core/utils/usecase.dart';
import 'package:banijet_flutter/feature/authentication/domain/usecases/get_profile.dart';
import 'package:meta/meta.dart';

class GetProfileCubit extends RequestCubit<NoParams> {
  final GetProfile getProfile;

  GetProfileCubit({@required this.getProfile});

  @override
  Future<void> call(NoParams params) async {
    emit(RequestLoading());

    final result = await getProfile(params);

    emit(result.fold(
            (failure) => RequestError(message: mapFailureToMessage(failure)),
            (data) => RequestSuccess(result: data)));
  }
}
