part of 'timer_cubit.dart';

abstract class TimerState extends Equatable {}

class TimerInactive extends TimerState {
  @override
  List<Object> get props => [];
}

class TimerActive extends TimerState {
  final int duration;

  TimerActive(this.duration);

  @override
  List<Object> get props => [duration];
}
