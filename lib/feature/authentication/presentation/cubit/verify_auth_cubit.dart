import 'package:banijet_flutter/core/cubit/request_cubit.dart';
import 'package:banijet_flutter/core/error/failures.dart';
import 'package:banijet_flutter/feature/authentication/domain/usecases/verify_authentication.dart';
import 'package:meta/meta.dart';

class VerifyAuthCubit extends RequestCubit<AuthVerifyParams> {
  final VerifyAuthentication verifyAuthentication;

  VerifyAuthCubit({@required this.verifyAuthentication});

  @override
  Future<void> call(AuthVerifyParams params) async {
    emit(RequestLoading());

    final result = await verifyAuthentication(params);

    emit(result.fold(
        (failure) => RequestError(message: mapFailureToMessage(failure)),
        (data) => RequestSuccess(result: data)));
  }
}
