import 'package:banijet_flutter/core/cubit/request_cubit.dart';
import 'package:meta/meta.dart';

import '../../../../core/error/failures.dart';
import '../../domain/usecases/request_authentication.dart';

class RequestAuthCubit extends RequestCubit<AuthRequestParam> {
  final RequestAuthentication requestAuthentication;

  RequestAuthCubit({@required this.requestAuthentication});

  @override
  Future<void> call(AuthRequestParam params) async {
    emit(RequestLoading());

    final result = await requestAuthentication(params);

    emit(result.fold(
        (failure) => RequestError(message: mapFailureToMessage(failure)),
        (data) => RequestSuccess(result: data)));
  }
}
