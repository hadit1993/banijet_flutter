import 'package:banijet_flutter/core/error/exception.dart';
import 'package:banijet_flutter/core/error/failures.dart';
import 'package:banijet_flutter/core/network/network_info.dart';
import 'package:banijet_flutter/feature/authentication/data/datasources/authentication_local_datasource.dart';
import 'package:banijet_flutter/feature/authentication/data/datasources/authentication_remote_datasource.dart';
import 'package:meta/meta.dart';
import 'package:banijet_flutter/feature/authentication/domain/entities/carrier.dart';

import 'package:dartz/dartz.dart';

import '../../domain/repository/authentication_repository.dart';

class AuthenticationRepositoryImpl implements AuthenticationRepository {
  final AuthenticationRemoteDataSource remoteDataSource;
  final AuthenticationLocalDataSource localDataSource;
  final NetworkInfo networkInfo;


  AuthenticationRepositoryImpl({
    @required this.remoteDataSource,
    @required this.localDataSource,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, Carrier>> getProfile() async {
    final isConnected = await networkInfo.isConnected;
    try {
      if (isConnected) {
        String token = await localDataSource.getAuthenticationToken();
        final profile = await remoteDataSource.getProfile(token);
        await localDataSource.cacheProfile(profile);
        return Right(profile);
      } else {
        final cachedProfile = await localDataSource.getCachedProfile();
        return Right(cachedProfile);
      }
    } catch (e) {
      if (e is ServerException) {
        return Left(ServerFailure(e.statusCode, e.message));
      } else if(e is CacheException) {
        return Left(CacheFailure());
      } else return Left(UnKnownFailure([e]));
    }

  }

  @override
  Future<Either<Failure, int>> requestAuthentication(
      String phoneNumber) async {
    final isConnected = await networkInfo.isConnected;
    try {
      if (isConnected) {
        final exp = await remoteDataSource.requestAuthentication(phoneNumber);
        return Right(exp);
      } else
        throw NoInternetException();
    } on NoInternetException {
      return Left(NoInternetFailure());
    } catch (e) {
      if (e is ServerException) {
        return Left(ServerFailure(e.statusCode, e.message));
      } else return Left(UnKnownFailure([e]));
    }
  }

  @override
  Future<Either<Failure, String>> verifyAuthentication(
      String phoneNumber, String verificationCode) async {
    final isConnected = await networkInfo.isConnected;
    try {
      if (isConnected) {
        final token = await remoteDataSource.verifyAuthentication(
            phoneNumber, verificationCode);
        await localDataSource.saveAuthenticationToken(token);
        return Right(token);
      } else
        throw NoInternetException();
    } on NoInternetException {
      return Left(NoInternetFailure());
    } catch (e) {
      if (e is ServerException) {
        return Left(ServerFailure(e.statusCode, e.message));
      } else return Left(UnKnownFailure([e]));
    }
  }

  @override
  Future<Either<Failure, bool>> signOut() async {
    await localDataSource.deleteAuthenticationToken();
    return Right(true);
  }

  @override
  Future<Either<Failure, bool>> isAuthenticated() async {
    final result = await localDataSource.hasToken();
    return Right(result);
  }
}
