import '../../domain/entities/carrier.dart';
import 'package:meta/meta.dart';

class CarrierModel extends Carrier {
  CarrierModel(
      {@required int id,
      @required String firstName,
      @required String lastName,
      @required String phone})
      : super(id: id, firstName: firstName, lastName: lastName, phone: phone);

  factory CarrierModel.fromJson(Map<String, dynamic> json) {
    return CarrierModel(
        id: json['id'],
        firstName: json['first_name'],
        lastName: json['last_name'],
        phone: json['phone']);
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'first_name': firstName,
      'last_name': lastName,
      'phone': phone
    };
  }
}
