import 'package:banijet_flutter/core/constants/path.dart';
import 'package:banijet_flutter/core/utils/http_client.dart';
import 'package:banijet_flutter/feature/authentication/data/models/carrier_model.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

abstract class AuthenticationRemoteDataSource {
  Future<int> requestAuthentication(String phoneNumber);

  Future<String> verifyAuthentication(
      String phoneNumber, String verificationCode);

  Future<CarrierModel> getProfile(String token);
}

class AuthenticationRemoteDataSourceImpl
    implements AuthenticationRemoteDataSource {
  final HttpClient client;

  AuthenticationRemoteDataSourceImpl({@required this.client});

  @override
  Future<CarrierModel> getProfile(String token) async {
    final result =
        await client.get(UrlPath.CARRIER, headers: {'Authorization': 'bearer $token'});

    return CarrierModel.fromJson(result);
  }

  @override
  Future<int> requestAuthentication(String phoneNumber) async {
    final result = await client.post(UrlPath.REQUEST_AUTH,
        headers: {Headers.contentTypeHeader: Headers.formUrlEncodedContentType},
        data: {'phone_number': phoneNumber});
    return result['exp'];
  }

  @override
  Future<String> verifyAuthentication(
      String phoneNumber, String verificationCode) async {
    final result = await client.post(UrlPath.VERIFY_AUTH, headers: {
      Headers.contentTypeHeader: Headers.formUrlEncodedContentType
    }, data: {
      'phone_number': phoneNumber,
      'verification_code': verificationCode
    });
    return result['token'];
  }
}
