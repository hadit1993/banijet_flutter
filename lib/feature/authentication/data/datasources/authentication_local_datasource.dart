import 'dart:convert';

import 'package:banijet_flutter/core/error/exception.dart';
import 'package:banijet_flutter/feature/authentication/data/models/carrier_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:meta/meta.dart';

const AUTH_TOKEN_KEY = 'Authentication Token';
const CACHED_CARRIER_KEY = "Cached Carrier";

abstract class AuthenticationLocalDataSource {
  Future<CarrierModel> getCachedProfile();

  Future<void> cacheProfile(CarrierModel profile);

  Future<void> saveAuthenticationToken(String token);

  Future<String> getAuthenticationToken();

  Future<void> deleteAuthenticationToken();

  Future<bool> hasToken();
}

class AuthenticationLocalDataSourceImpl
    implements AuthenticationLocalDataSource {
  final SharedPreferences sharedPreferences;

  AuthenticationLocalDataSourceImpl({@required this.sharedPreferences});

  @override
  Future<void> cacheProfile(CarrierModel profile) {

    return sharedPreferences.setString(CACHED_CARRIER_KEY, json.encode(profile.toJson()));
  }

  @override
  Future<void> deleteAuthenticationToken() {
    return sharedPreferences.remove(AUTH_TOKEN_KEY);
  }

  @override
  Future<String> getAuthenticationToken() {
    final token = sharedPreferences.getString(AUTH_TOKEN_KEY);
    if (token != null) {
      return Future.value(token);
    } else
      throw CacheException();
  }

  @override
  Future<CarrierModel> getCachedProfile() {
    final jsonString = sharedPreferences.getString(CACHED_CARRIER_KEY);
    if(jsonString != null)
    return Future.value(CarrierModel.fromJson(json.decode(jsonString)));
    else throw CacheException();
  }

  @override
  Future<void> saveAuthenticationToken(String token) {
    return sharedPreferences.setString(AUTH_TOKEN_KEY, token);
  }

  @override
  Future<bool> hasToken() {
    final token = sharedPreferences.getString(AUTH_TOKEN_KEY);
    return Future.value(token != null);
  }
}
