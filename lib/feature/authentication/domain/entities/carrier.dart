
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class Carrier extends Equatable {
  final int id;
  final String firstName;
  final String lastName;
  final String phone;

  Carrier({
    @required this.id,
    @required this.firstName,
    @required this.lastName,
    @required this.phone
});


  @override

  List<Object> get props => [id,firstName, lastName, phone];

}