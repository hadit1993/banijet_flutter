import 'package:banijet_flutter/core/error/failures.dart';
import 'package:banijet_flutter/feature/authentication/domain/entities/carrier.dart';
import 'package:dartz/dartz.dart';

abstract class AuthenticationRepository {
  Future<Either<Failure, int>> requestAuthentication(String phoneNumber);

  Future<Either<Failure, String>> verifyAuthentication(
      String phoneNumber, String verificationCode);

  Future<Either<Failure, Carrier>> getProfile();

  Future<Either<Failure, bool>> signOut();

  Future<Either<Failure, bool>> isAuthenticated();

}
