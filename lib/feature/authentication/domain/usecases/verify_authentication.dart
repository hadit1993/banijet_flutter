import 'package:banijet_flutter/core/error/failures.dart';
import 'package:banijet_flutter/core/utils/usecase.dart';
import 'package:banijet_flutter/feature/authentication/domain/repository/authentication_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class VerifyAuthentication extends UseCase<String, AuthVerifyParams> {

  final AuthenticationRepository repository;

  VerifyAuthentication(this.repository);

  @override
  Future<Either<Failure, String>> call(AuthVerifyParams params) {
    return repository.verifyAuthentication(params.phoneNumber, params.verificationCode);
  }
}

class AuthVerifyParams extends Equatable {
  final String phoneNumber;
  final String verificationCode;

  AuthVerifyParams({
    @required this.phoneNumber,
    @required this.verificationCode
});

  @override
  List<Object> get props => [phoneNumber, verificationCode];
}
