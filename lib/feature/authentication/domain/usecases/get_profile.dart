


import 'package:banijet_flutter/core/error/failures.dart';
import 'package:banijet_flutter/core/utils/usecase.dart';
import 'package:banijet_flutter/feature/authentication/domain/entities/carrier.dart';
import 'package:banijet_flutter/feature/authentication/domain/repository/authentication_repository.dart';
import 'package:dartz/dartz.dart';

class GetProfile extends UseCase<Carrier, NoParams> {

  final AuthenticationRepository repository;

  GetProfile(this.repository);

  @override
  Future<Either<Failure, Carrier>> call(NoParams params) {

    return repository.getProfile();
  }

}