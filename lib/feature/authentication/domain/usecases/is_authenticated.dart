import 'package:banijet_flutter/core/error/failures.dart';
import 'package:banijet_flutter/core/utils/usecase.dart';
import 'package:banijet_flutter/feature/authentication/domain/repository/authentication_repository.dart';
import 'package:dartz/dartz.dart';

class IsAuthenticated extends UseCase<bool, NoParams> {
  final AuthenticationRepository repository;

  IsAuthenticated(this.repository);

  @override
  Future<Either<Failure, bool>> call(NoParams params) {
    return repository.isAuthenticated();
  }
}
