import 'package:banijet_flutter/core/error/failures.dart';
import 'package:banijet_flutter/core/utils/usecase.dart';
import 'package:banijet_flutter/feature/authentication/domain/repository/authentication_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';


class RequestAuthentication extends UseCase<int, AuthRequestParam> {
 final AuthenticationRepository repository;

 RequestAuthentication(this.repository);

  @override
  Future<Either<Failure, int>> call(AuthRequestParam params)  {
    return repository.requestAuthentication(params.phoneNumber);
  }


}

class AuthRequestParam extends Equatable {
  final String phoneNumber;

  AuthRequestParam({@required this.phoneNumber});

  @override

  List<Object> get props => [phoneNumber];

}