import 'package:bloc/bloc.dart';

class CubitObserver extends BlocObserver {
  @override
  void onChange(Cubit cubit, Change change) {
    print('$cubit changed: $change');
    print('state: ${cubit.state}');
    super.onChange(cubit, change);
  }

  @override
  void onError(Cubit cubit, Object error, StackTrace stackTrace) {
    print('$cubit occurred error: $error $stackTrace');
    super.onError(cubit, error, stackTrace);
  }
}
