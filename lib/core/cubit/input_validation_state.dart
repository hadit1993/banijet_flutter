part of 'input_validation_cubit.dart';

@immutable
abstract class InputValidationState extends Equatable {

  final String input;

  InputValidationState({@required this.input});

  @override
  List<Object> get props => [input];
}

class InvalidInput extends InputValidationState {
  InvalidInput({@required String input}) : super(input: input);
}

class ValidInput extends InputValidationState {

  ValidInput({@required String input}) : super(input: input);
}


