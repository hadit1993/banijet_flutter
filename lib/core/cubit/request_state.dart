part of 'request_cubit.dart';

@immutable
abstract class RequestState extends Equatable {
  final List stateProps;

  RequestState([this.stateProps = const <dynamic>[]]);

  @override
  List<Object> get props => this.stateProps;
}

class RequestInitial extends RequestState {}

class RequestLoading extends RequestState {}

class RequestSuccess<T> extends RequestState {
  final T result;

  RequestSuccess({this.result}) : super([result]);
}

class RequestError extends RequestState {
  final String message;

  RequestError({@required this.message}) : super([message]);
}
