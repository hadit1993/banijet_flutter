import 'package:banijet_flutter/core/utils/input_validator.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'input_validation_state.dart';

enum InputType { Phone, VerificationCode }

class InputValidationCubit extends Cubit<InputValidationState> {
  final InputType inputType;
  final InputValidator inputValidator;

  InputValidationCubit({
    @required this.inputType,
    @required this.inputValidator,
  }) : super(InvalidInput(input: ''));

  void validate(String input) {
    if (inputType == InputType.Phone) {
      emit(inputValidator.isPhoneValid(input)
          ? ValidInput(input: input)
          : InvalidInput(input: input ?? ''));
    } else
      emit(inputValidator.isVerificationCodeValid(input)
          ? ValidInput(input: input)
          : InvalidInput(input: input ?? ''));
  }
}
