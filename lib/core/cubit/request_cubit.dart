import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';



part 'request_state.dart';

abstract class RequestCubit<Params> extends Cubit<RequestState> {
  RequestCubit() : super(RequestInitial());

  Future<void> call(Params params);

  void reset() {
    emit(RequestInitial());
  }
}
