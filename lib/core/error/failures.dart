import 'package:equatable/equatable.dart';

import '../constants/errors.dart';

abstract class Failure extends Equatable {
  final List properties;

  Failure([this.properties = const <dynamic>[]]);

  @override
  List<Object> get props => this.properties;
}

class ServerFailure extends Failure {
  final int statusCode;
  final String message;

  ServerFailure(this.statusCode, this.message) : super([statusCode, message]);
}

class CacheFailure extends Failure {}

class UnKnownFailure extends Failure {
  UnKnownFailure(List properties) : super(properties);
}

class NoInternetFailure extends Failure {}

class InvalidInputFailure extends Failure {
  final String message;

  InvalidInputFailure(this.message) : super([message]);
}

String mapFailureToMessage(Failure failure) {

  if (failure is ServerFailure) return failure.message;
  else if (failure is CacheFailure) return Errors.CACHE_FAILURE_ERROR;
  else if (failure is NoInternetFailure) return Errors.NO_INTERNET_FAILURE_ERROR;
  else return Errors.UNKNOWN_FAILURE_ERROR;
}
