import 'dart:convert';

import 'package:banijet_flutter/core/error/exception.dart';
import 'package:banijet_flutter/core/models/base_response.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

class HttpClient {
  final Dio _dio;

  HttpClient(this._dio);

  Future<Map<String, dynamic>> get(String path,
      {Map<String, dynamic> queryParams, Map<String, dynamic> headers}) async {
    try {
      final Response response = await _dio.get(path,
          queryParameters: queryParams,
          options: headers != null ? Options(headers: headers) : null);

      final baseResponse = BaseResponse.fromJson(response.data);
      if (baseResponse.status == 'success')
        return baseResponse.data;
      else {
        throw ServerException(
            statusCode: baseResponse.statusCode,
            message: baseResponse.data != null
                ? baseResponse.data[0]
                : baseResponse.message);
      }
    } on DioError catch (e) {
      _handleDioError(e);
      return null;
    }
  }

  dynamic post(String path,
      {@required dynamic data, Map<String, dynamic> headers}) async {
    try {
      final Response response = await _dio.post(path,
          data: data,
          options: headers != null ? Options(headers: headers) : null);
      print('response: ${response}');
      print('type: ${response.data.runtimeType}');

      final baseResponse = BaseResponse.fromJson(response.data);
      if (baseResponse.status == 'success')
        return baseResponse.data;
      else {
        throw ServerException(
            statusCode: baseResponse.statusCode,
            message: baseResponse.data != null
                ? baseResponse.data[0]
                : baseResponse.message);
      }
    } on DioError catch (e) {
      _handleDioError(e);
      return null;
    }
  }

  dynamic put(String path, {dynamic data, Map<String, dynamic> headers}) {}

  dynamic delete(String path, {dynamic data, Map<String, dynamic> headers}) {}

  void _handleDioError(DioError error) {
    if (error.response != null) {
      print(error.response.data);
      print(error.response.headers);
      print(error.response.request);
      throw ServerException(
          statusCode: error.response.statusCode, message: error.message);
    } else {
      print(error.request);
      print(error.message);
      throw ServerException(statusCode: 0, message: error.message);
    }
  }
}
