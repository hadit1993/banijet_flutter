class InputValidator {
  bool isDigit(String input) {
    RegExp _numeric = RegExp(r'^-?[0-9]+$');
    return _numeric.hasMatch(input);
  }

  bool isPhoneValid(String input) {
    return input != null &&
        isDigit(input) &&
        input.startsWith("09") &&
        input.length == 11;
  }

  bool isVerificationCodeValid(String input) {
    return input != null && isDigit(input) && input.length == 5;
  }
}
