import 'package:banijet_flutter/core/constants/path.dart';
import 'package:banijet_flutter/core/cubit/cubit_observer.dart';
import 'package:banijet_flutter/core/network/network_info.dart';
import 'package:banijet_flutter/core/utils/http_client.dart';
import 'package:banijet_flutter/core/utils/input_validator.dart';
import 'package:banijet_flutter/feature/authentication/data/datasources/authentication_local_datasource.dart';
import 'package:banijet_flutter/feature/authentication/data/datasources/authentication_remote_datasource.dart';
import 'package:banijet_flutter/feature/authentication/data/repository/authentication_repository_impl.dart';
import 'package:banijet_flutter/feature/authentication/domain/repository/authentication_repository.dart';
import 'package:banijet_flutter/feature/authentication/domain/usecases/get_profile.dart';
import 'package:banijet_flutter/feature/authentication/domain/usecases/is_authenticated.dart';
import 'package:banijet_flutter/feature/authentication/domain/usecases/request_authentication.dart';
import 'package:banijet_flutter/feature/authentication/domain/usecases/signout.dart';
import 'package:banijet_flutter/feature/authentication/domain/usecases/verify_authentication.dart';
import 'package:banijet_flutter/feature/authentication/presentation/cubit/get_profile_cubit.dart';
import 'package:banijet_flutter/feature/authentication/presentation/cubit/request_auth_cubit.dart';
import 'package:banijet_flutter/feature/authentication/presentation/cubit/timer_cubit.dart';
import 'package:banijet_flutter/feature/authentication/presentation/cubit/verify_auth_cubit.dart';
import 'package:banijet_flutter/feature/authentication/util/ticker.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

final sl = GetIt.instance;

Future<void> init() async {
  //cubits
  sl.registerFactory(() => RequestAuthCubit(requestAuthentication: sl()));
  sl.registerFactory(() => VerifyAuthCubit(verifyAuthentication: sl()));
  sl.registerFactory(() => GetProfileCubit(getProfile: sl()));
  sl.registerFactory(() => TimerCubit(ticker: sl()));

  //usecases
  sl.registerLazySingleton(() => RequestAuthentication(sl()));
  sl.registerLazySingleton(() => VerifyAuthentication(sl()));
  sl.registerLazySingleton(() => GetProfile(sl()));
  sl.registerLazySingleton(() => IsAuthenticated(sl()));
  sl.registerLazySingleton(() => SignOut(sl()));

  //auth utils
  sl.registerLazySingleton(() => Ticker());

  //repository
  sl.registerLazySingleton<AuthenticationRepository>(() =>
      AuthenticationRepositoryImpl(
          remoteDataSource: sl(), localDataSource: sl(), networkInfo: sl()));

  //Data Sources
  sl.registerLazySingleton<AuthenticationRemoteDataSource>(
      () => AuthenticationRemoteDataSourceImpl(client: sl()));

  sl.registerLazySingleton<AuthenticationLocalDataSource>(
      () => AuthenticationLocalDataSourceImpl(sharedPreferences: sl()));

  //core network
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

  //external
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => HttpClient(sl()));
  final options = BaseOptions(baseUrl: UrlPath.BASE_URL);
  final dio = Dio(options);
  sl.registerLazySingleton(() => dio);
  sl.registerLazySingleton(() => DataConnectionChecker());
  sl.registerLazySingleton(() => InputValidator());



}
