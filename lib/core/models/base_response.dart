import 'package:meta/meta.dart';

class BaseResponse {
  final String status;
  final int statusCode;
  final String message;
  final dynamic data;

  BaseResponse(
      {@required this.status,
      @required this.statusCode,
      @required this.message,
      @required this.data});

  factory BaseResponse.fromJson(Map<String, dynamic> json) {
    return BaseResponse(
        status: json['status'],
        statusCode: json['status_code'],
        message: json['message'],
        data: json['data']);
  }

  Map<String, dynamic> toJson() {
    return {
      'status': status,
      'status_code': statusCode,
      'message': message,
      'data': data
    };
  }
}
