import 'dart:ui';



const ERROR_COLOR = Color(0xffff4d54);

class BanijetColors {

  static const PRIMARY = Color(0xff1ac977);
  static const DISABLED_COLOR = Color(0xffc5c7cb);
  static const ERROR_COLOR = Color(0xffff4d54);

}