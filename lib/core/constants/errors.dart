class Errors {
  static const INVALID_PHONE_NUMBER_ERROR = 'شماره تلفن شما نامعتبر است';

  static const CACHE_FAILURE_ERROR =
      'ارتباط با حافظه دستگاه با اختلال مواجه شد';

  static const NO_INTERNET_FAILURE_ERROR = 'دستگاه شما به اینترنت متصل نیست';

  static const UNKNOWN_FAILURE_ERROR = 'خطای ناشناخته: لطفا دوباره تلاش کنید';
}
