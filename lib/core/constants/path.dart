class UrlPath {
  static const BASE_URL = 'https://banijetapi.banimode.com/api';

  static const REQUEST_AUTH = '/auth/request';

  static const VERIFY_AUTH = '/auth';

  static const CARRIER = '/profile';
}
