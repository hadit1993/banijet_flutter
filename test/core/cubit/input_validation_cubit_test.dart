import 'package:banijet_flutter/core/cubit/input_validation_cubit.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../mocks/mocks.dart';

void main() {
  MockInputValidator validator;
  final tPhone = '09361958846';

  setUp(() {
    validator = MockInputValidator();
  });

  group('test input validator', () {
    blocTest<InputValidationCubit, InputValidationState>(
        'should emit [ValidInput] is input is valid',
        build: () {
          when(validator.isPhoneValid(any)).thenReturn(true);
          return InputValidationCubit(
              inputType: InputType.Phone, inputValidator: validator);
        },
        act: (c) => c.validate(tPhone),
        verify: (_) {
          verify(validator.isPhoneValid(tPhone));
        },
        expect: [ValidInput(input: tPhone)]);

    blocTest<InputValidationCubit, InputValidationState>(
        'should emit [InvalidInput()] is input is invalid',
        build: () {
          when(validator.isPhoneValid(any)).thenReturn(false);
          return InputValidationCubit(
              inputType: InputType.Phone, inputValidator: validator);
        },
        act: (c) => c.validate(tPhone),
        verify: (_) {
          verify(validator.isPhoneValid(tPhone));
        },
        expect: [InvalidInput(input: tPhone)]);
  });
}
