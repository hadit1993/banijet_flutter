

import 'package:banijet_flutter/core/utils/input_validator.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

void main() {
  InputValidator inputValidator;

  setUp(() {
    inputValidator = InputValidator();
  });

  group('test isDigit function', () {

    test('should return true if the string is digit', () {

      final String input = "12345";
      final result = inputValidator.isDigit(input);

      expect(result, true);

    });
    test('should return false if the string is is not fully digit', () {

      final String input = "123abcd45";
      final result = inputValidator.isDigit(input);

      expect(result, false);

    });

  });

  group('test isPhoneValid function', (){

    test('should return true if the phone number is valid', () {

      final phone = "09361958846";
      final result = inputValidator.isPhoneValid(phone);
      expect(result, true);

    });

    test('should return false if the input has non digit characters', () {

      final phone = "0936A958846";
      final result = inputValidator.isPhoneValid(phone);
      expect(result, false);

    });

    test('should return false if the input does not start with 09', () {

      final phone = "05361A58846";
      final result = inputValidator.isPhoneValid(phone);
      expect(result, false);

    });
    test('should return false if the input does not have enough length', () {

      final phone = "0936195884";
      final result = inputValidator.isPhoneValid(phone);
      expect(result, false);

    });
  });

  group('test isVerificationCodeValid', (){

    test('should return true if the input is valid', () {
      final input = '12345';
      final result = inputValidator.isVerificationCodeValid(input);
      expect(result, true);

    });

    test('should return false if the input length is not enough', () {
      final input = '1234';
      final result = inputValidator.isVerificationCodeValid(input);
      expect(result, false);

    });
    test('should return false if the input is not fully digit', () {
      final input = '1234a';
      final result = inputValidator.isVerificationCodeValid(input);
      expect(result, false);

    });
  });
}