import 'dart:convert';

import 'package:banijet_flutter/core/constants/path.dart';
import 'package:banijet_flutter/core/error/exception.dart';
import 'package:banijet_flutter/core/models/base_response.dart';
import 'package:banijet_flutter/core/utils/http_client.dart';
import 'package:banijet_flutter/feature/authentication/data/models/carrier_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import '../mocks/mocks.dart';
import '../../fixtures/fixture_reader.dart';
import 'package:matcher/matcher.dart';

void main() {
  HttpClient client;
  Dio dio;
  DioAdapterMock adapterMock;
  final tToken = 'test';
  final tHeaders = {'Authorization': 'bearer $tToken'};
  final tCarrierModel = CarrierModel(
      id: 1, firstName: 'Hadi', lastName: 'Tahmasbi', phone: '09361958846');

  setUp(() {
    adapterMock = DioAdapterMock();
    dio = Dio();
    dio.httpClientAdapter = adapterMock;
    client = HttpClient(dio);
  });

  group('test get api', () {

    test('should return data successfully', () async {
      final payload = fixture('success_response.json');
      final httpResponse = ResponseBody.fromString(
        payload,
        200,
        headers: {
          Headers.contentTypeHeader: [Headers.jsonContentType],
          'Authorization': ['bearer $tToken']
        },
      );

      when(adapterMock.fetch(any, any, any))
          .thenAnswer((_) async => httpResponse);

      final result = await client.get(UrlPath.CARRIER, headers: tHeaders);

      expect(result, equals(tCarrierModel.toJson()));
    });

    test('should throw ServerException with 200 status code', () async {
      final payload = fixture('error_response.json');
      final httpResponse = ResponseBody.fromString(
        payload,
        200,
        headers: {
          Headers.contentTypeHeader: [Headers.jsonContentType]
        },
      );

      when(adapterMock.fetch(any, any, any))
          .thenAnswer((_) async => httpResponse);

      final call = client.get;
      expect(() => call(UrlPath.CARRIER, headers: tHeaders),
          throwsA(TypeMatcher<ServerException>()));
    });

    test('should throw ServerException with 403 status code', () async {
      final res = {'status': 'success', 'data': ''};
      final httpResponse = ResponseBody.fromString(
        json.encode(res),
        403,
        headers: {
          Headers.contentTypeHeader: [Headers.jsonContentType]
        },
      );

      when(adapterMock.fetch(any, any, any))
          .thenAnswer((_) async => httpResponse);

      final call = client.get;
      expect(() => call(UrlPath.CARRIER, headers: tHeaders),
          throwsA(TypeMatcher<ServerException>()));
    });
  });

  group('test post api', () {



    test('should return data successfully', () async {
      final payload = fixture('success_request_auth_response.json');
      // final payload = fixture('success_response.json');
      final httpResponse = ResponseBody.fromString(
        payload,
        200,
        headers: {
          Headers.contentTypeHeader: [Headers.formUrlEncodedContentType]
        },
      );

      when(adapterMock.fetch(any, any, any))
          .thenAnswer((_) async => httpResponse);

      final result = await client
          .post(UrlPath.REQUEST_AUTH, data: {'phone_number': '09361958846'});

      expect(result, equals({'exp': '120'}));
      // expect(result, equals(tCarrierModel.toJson()));
    });

    test('should throw ServerException with 200 status code', () async {
      final payload = fixture('error_response.json');
      final httpResponse = ResponseBody.fromString(
        payload,
        200,
        headers: {
          Headers.contentTypeHeader: [Headers.formUrlEncodedContentType]
        },
      );

      when(adapterMock.fetch(any, any, any))
          .thenAnswer((_) async => httpResponse);

      final call = client.post;
      expect(() => call(UrlPath.REQUEST_AUTH, data: {'phone_number': '09361958846'}),
          throwsA(TypeMatcher<ServerException>()));
    });

    test('should throw ServerException with 403 status code', () async {
      final res = {'status': 'success', 'data': ''};
      final httpResponse = ResponseBody.fromString(
        json.encode(res),
        403,
        headers: {
          Headers.contentTypeHeader: [Headers.formUrlEncodedContentType]
        },
      );

      when(adapterMock.fetch(any, any, any))
          .thenAnswer((_) async => httpResponse);

      final call = client.post;
      expect(() => call(UrlPath.REQUEST_AUTH, data: {'phone_number': '09361958846'}),
          throwsA(TypeMatcher<ServerException>()));
    });
  });
}
