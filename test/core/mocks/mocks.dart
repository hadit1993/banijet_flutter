import 'package:banijet_flutter/core/cubit/input_validation_cubit.dart';
import 'package:banijet_flutter/core/cubit/request_cubit.dart';
import 'package:banijet_flutter/core/network/network_info.dart';
import 'package:banijet_flutter/core/utils/http_client.dart';
import 'package:banijet_flutter/core/utils/input_validator.dart';
import 'package:banijet_flutter/feature/authentication/data/datasources/authentication_local_datasource.dart';
import 'package:banijet_flutter/feature/authentication/data/datasources/authentication_remote_datasource.dart';
import 'package:banijet_flutter/feature/authentication/domain/usecases/get_profile.dart';
import 'package:banijet_flutter/feature/authentication/domain/usecases/request_authentication.dart';
import 'package:banijet_flutter/feature/authentication/domain/usecases/verify_authentication.dart';
import 'package:banijet_flutter/feature/authentication/presentation/cubit/request_auth_cubit.dart';
import 'package:banijet_flutter/feature/authentication/util/ticker.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/cupertino.dart';
import 'package:mockito/mockito.dart';
import 'package:banijet_flutter/feature/authentication/domain/repository/authentication_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:dio/dio.dart';

class MockAuthenticationRepository extends Mock
    implements AuthenticationRepository {}

class MockAuthRemoteDataSource extends Mock
    implements AuthenticationRemoteDataSource {}

class MockAuthLocalDataSource extends Mock
    implements AuthenticationLocalDataSource {}

class MockNetworkInfo extends Mock implements NetworkInfo {}

class MockSharedPreferences extends Mock implements SharedPreferences {}

class MockHttpClient extends Mock implements HttpClient {}

class MockDio extends Mock implements Dio {}

class DioAdapterMock extends Mock implements HttpClientAdapter {}

class MockRequestAuthentication extends Mock implements RequestAuthentication {}

class MockInputValidator extends Mock implements InputValidator {}

class MockVerifyAuthentication extends Mock implements VerifyAuthentication {}

class MockTicker extends Mock implements Ticker {}

class MockGetProfile extends Mock implements GetProfile {}

class MockDataConnectionChecker extends Mock implements DataConnectionChecker {}

class MockInputValidationCubit extends MockBloc<InputValidationState> implements InputValidationCubit {}

class MockRequestAuthCubit extends MockBloc<RequestState> implements RequestAuthCubit {}

class MockTextController extends Mock implements TextEditingController {}
