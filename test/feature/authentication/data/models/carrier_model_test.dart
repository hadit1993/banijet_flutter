import 'dart:convert';

import 'package:banijet_flutter/feature/authentication/data/models/carrier_model.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../fixtures/fixture_reader.dart';


void main() {
  final tCarrierModel = CarrierModel(
      id: 1, firstName: 'Hadi', lastName: 'Tahmasbi', phone: '09361958846');

 group('json conversion', () {
   test('should be subclass of Carrier entity', () async {

     final Map<String, dynamic> jsonMap = json.decode(fixture('carrier.json'));
     final result = CarrierModel.fromJson(jsonMap);

     expect(result, tCarrierModel);
   });

   test('should return a json map containing proper data', () {

     final result = tCarrierModel.toJson();
     final expectedJsonMap = {
       "id": 1,
       "first_name": "Hadi",
       "last_name": "Tahmasbi",
       "phone": "09361958846"
     };

     expect(result, expectedJsonMap);
   });
 });
}
