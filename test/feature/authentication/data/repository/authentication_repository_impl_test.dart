import 'package:banijet_flutter/core/error/exception.dart';
import 'package:banijet_flutter/core/error/failures.dart';
import 'package:banijet_flutter/feature/authentication/data/models/carrier_model.dart';
import 'package:banijet_flutter/feature/authentication/data/repository/authentication_repository_impl.dart';
import 'package:banijet_flutter/feature/authentication/domain/entities/carrier.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import '../../../../core/mocks/mocks.dart';

void main() {
  AuthenticationRepositoryImpl repositoryImpl;
  MockAuthRemoteDataSource remoteDataSource;
  MockAuthLocalDataSource localDataSource;
  MockNetworkInfo networkInfo;

  final tPhoneNumber = '09361958846';
  final tVerificationCode = '12345';
  final tExp = 120;
  final tToken = 'test';
  final tCarrierModel = CarrierModel(
      id: 1, firstName: 'Hadi', lastName: 'Tahmasbi', phone: tPhoneNumber);
  final Carrier tCarrier = tCarrierModel;
  final tStatusCode = 400;
  final tRemoteMessage = 'bad request';

  setUp(() {
    remoteDataSource = MockAuthRemoteDataSource();
    localDataSource = MockAuthLocalDataSource();
    networkInfo = MockNetworkInfo();
    repositoryImpl = AuthenticationRepositoryImpl(
        remoteDataSource: remoteDataSource,
        localDataSource: localDataSource,
        networkInfo: networkInfo);
  });

  group('tests authentication when device is offline', () {
    setUp(() {
      when(networkInfo.isConnected).thenAnswer((_) async => false);
    });
    test(
        'should return no internet failure when there is no connection on request authentication',
        () async {
      final result = await repositoryImpl.requestAuthentication(tPhoneNumber);
      verifyZeroInteractions(remoteDataSource);

      expect(result, equals(Left(NoInternetFailure())));
    });

    test(
        'should return no internet failure when there is no connection on verify authentication',
        () async {
      final result = await repositoryImpl.verifyAuthentication(
          tPhoneNumber, tVerificationCode);
      verifyZeroInteractions(remoteDataSource);

      expect(result, equals(Left(NoInternetFailure())));
    });

    test(
        'should return cached profile if there is one when there is no connection on getting profile',
        () async {
      when(localDataSource.getCachedProfile())
          .thenAnswer((_) async => tCarrierModel);

      final result = await repositoryImpl.getProfile();

      verify(localDataSource.getCachedProfile());
      verifyZeroInteractions(remoteDataSource);
      expect(result, equals(Right(tCarrier)));
    });

    test('should return cache failure when there is no cached profile',
        () async {
      when(localDataSource.getCachedProfile()).thenThrow(CacheException());

      final result = await repositoryImpl.getProfile();

      verify(localDataSource.getCachedProfile());

      verifyZeroInteractions(remoteDataSource);

      expect(result, Left(CacheFailure()));
    });
  });

  group('test authentication when device is online', () {
    setUp(() {
      when(networkInfo.isConnected).thenAnswer((_) async => true);
    });

    test('should return timer when request authentication successfully',
        () async {
      when(remoteDataSource.requestAuthentication(tPhoneNumber))
          .thenAnswer((_) async => tExp);

      final result = await repositoryImpl.requestAuthentication(tPhoneNumber);

      verify(remoteDataSource.requestAuthentication(tPhoneNumber));

      expect(result, equals(Right(tExp)));
    });

    test('should return server failure when the request authentication failed ',
        () async {
      when(remoteDataSource.requestAuthentication(tPhoneNumber)).thenThrow(
          ServerException(statusCode: tStatusCode, message: tRemoteMessage));

      final result = await repositoryImpl.requestAuthentication(tPhoneNumber);

      verify(remoteDataSource.requestAuthentication(tPhoneNumber));

      expect(result, equals(Left(ServerFailure(tStatusCode, tRemoteMessage))));
    });

    test('should return token when verify authentication successfully',
        () async {
      when(remoteDataSource.verifyAuthentication(
              tPhoneNumber, tVerificationCode))
          .thenAnswer((_) async => tToken);

      final result = await repositoryImpl.verifyAuthentication(
          tPhoneNumber, tVerificationCode);

      verify(remoteDataSource.verifyAuthentication(
          tPhoneNumber, tVerificationCode));

      expect(result, equals(Right(tToken)));
    });

    test('should save token when verify authentication successfully', () async {
      when(remoteDataSource.verifyAuthentication(
              tPhoneNumber, tVerificationCode))
          .thenAnswer((_) async => tToken);

      await repositoryImpl.verifyAuthentication(
          tPhoneNumber, tVerificationCode);
      verify(remoteDataSource.verifyAuthentication(
          tPhoneNumber, tVerificationCode));
      verify(localDataSource.saveAuthenticationToken(tToken));
    });

    test('should return server failure when verifying authentication failed ',
        () async {
      when(remoteDataSource.verifyAuthentication(
              tPhoneNumber, tVerificationCode))
          .thenThrow(ServerException(
              statusCode: tStatusCode, message: tRemoteMessage));

      final result = await repositoryImpl.verifyAuthentication(
          tPhoneNumber, tVerificationCode);

      verify(remoteDataSource.verifyAuthentication(
          tPhoneNumber, tVerificationCode));

      expect(result, equals(Left(ServerFailure(tStatusCode, tRemoteMessage))));
    });

    test('should return profile when call to remote data source is successful',
        () async {
      when(localDataSource.getAuthenticationToken())
          .thenAnswer((_) async => tToken);
      when(remoteDataSource.getProfile(any))
          .thenAnswer((_) async => tCarrierModel);

      final result = await repositoryImpl.getProfile();

      verify(remoteDataSource.getProfile(tToken));

      expect(result, equals(Right(tCarrier)));
    });

    test(
        'should cache profile when the call to remote data source is successful',
        () async {
      when(localDataSource.getAuthenticationToken())
          .thenAnswer((_) async => tToken);
      when(remoteDataSource.getProfile(any))
          .thenAnswer((_) async => tCarrierModel);

      await repositoryImpl.getProfile();

      verify(localDataSource.getAuthenticationToken());

      verify(remoteDataSource.getProfile(tToken));

      verify(localDataSource.cacheProfile(tCarrier));
    });

    test('should return server failure when getting profile failed ', () async {
      when(localDataSource.getAuthenticationToken())
          .thenAnswer((_) async => tToken);
      when(remoteDataSource.getProfile(any)).thenThrow(
          ServerException(statusCode: tStatusCode, message: tRemoteMessage));

      final result = await repositoryImpl.getProfile();

      verify(remoteDataSource.getProfile(tToken));

      expect(result, equals(Left(ServerFailure(tStatusCode, tRemoteMessage))));
    });
  });

  test('sign out should performed', () async {
    await repositoryImpl.signOut();

    verify(localDataSource.deleteAuthenticationToken());
  });

  test('check if the user is authenticated', () async {
    when(localDataSource.hasToken()).thenAnswer((_) async => true);

    final result = await repositoryImpl.isAuthenticated();

    verify(localDataSource.hasToken());

    expect(result, equals(Right(true)));
  });
}
