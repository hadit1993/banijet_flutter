import 'dart:convert';

import 'package:banijet_flutter/core/constants/path.dart';
import 'package:banijet_flutter/core/error/exception.dart';
import 'package:banijet_flutter/feature/authentication/data/datasources/authentication_remote_datasource.dart';
import 'package:banijet_flutter/feature/authentication/data/models/carrier_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:matcher/matcher.dart';

import '../../../../core/mocks/mocks.dart';
import '../../../../fixtures/fixture_reader.dart';

void main() {
  AuthenticationRemoteDataSourceImpl remoteDataSourceImpl;
  MockHttpClient httpClient;
  final tPhoneNumber = '09361958846';
  final tVerificationCode = '12345';
  final tToken = 'test';
  final tHeader = {'Content-Type': Headers.formUrlEncodedContentType};

  setUp(() {
    httpClient = MockHttpClient();
    remoteDataSourceImpl =
        AuthenticationRemoteDataSourceImpl(client: httpClient);
  });

  group('request authentication', () {
    test(
        'should request authentication'
        ' successfully ', () async {
      when(httpClient.post(any,
              headers: anyNamed('headers'), data: anyNamed('data')))
          .thenAnswer((_) async => {'exp': '120'});

      await remoteDataSourceImpl.requestAuthentication(tPhoneNumber);

      verify(httpClient.post(UrlPath.REQUEST_AUTH,
          headers: tHeader, data: {'phone_number': tPhoneNumber}));
    });

    test('should return timer when request is successful', () async {
      when(httpClient.post(any,
              headers: anyNamed('headers'), data: anyNamed('data')))
          .thenAnswer((_) async => {'exp': '120'});

      final result =
          await remoteDataSourceImpl.requestAuthentication(tPhoneNumber);

      expect('120', equals(result));
    });

    test('should throw s ServerException when there is an error', () async {
      when(httpClient.post(any,
              headers: anyNamed('headers'), data: anyNamed('data')))
          .thenThrow(ServerException(statusCode: 404, message: 'not found'));
      final call = remoteDataSourceImpl.requestAuthentication;

      expect(() => call(tPhoneNumber), throwsA(TypeMatcher<ServerException>()));
    });
  });

  group('verify authentication', () {
    test(
        'should verify authentication'
        ' successfully ', () async {
      when(httpClient.post(any,
              headers: anyNamed('headers'), data: anyNamed('data')))
          .thenAnswer((_) async => {'token': tToken});

      await remoteDataSourceImpl.verifyAuthentication(
          tPhoneNumber, tVerificationCode);

      verify(httpClient.post(UrlPath.VERIFY_AUTH, headers: tHeader, data: {
        'phone_number': tPhoneNumber,
        'verification_code': tVerificationCode
      }));
    });

    test('should return token when request is successful', () async {
      when(httpClient.post(any,
              headers: anyNamed('headers'), data: anyNamed('data')))
          .thenAnswer((_) async => {'token': tToken});

      final result = await remoteDataSourceImpl.verifyAuthentication(
          tPhoneNumber, tVerificationCode);

      expect(tToken, equals(result));
    });

    test('should throw  ServerException when there is an error', () async {
      when(httpClient.post(any,
              headers: anyNamed('headers'), data: anyNamed('data')))
          .thenThrow(ServerException(statusCode: 404, message: 'not found'));
      final call = remoteDataSourceImpl.verifyAuthentication;

      expect(() => call(tPhoneNumber, tVerificationCode),
          throwsA(TypeMatcher<ServerException>()));
    });
  });

  group('get Profile', () {
    test(
        'should get profile'
        ' successfully ', () async {
      when(httpClient.get(any, headers: anyNamed('headers')))
          .thenAnswer((_) async => json.decode(fixture('carrier.json')));

      await remoteDataSourceImpl.getProfile(tToken);

      verify(httpClient
          .get(UrlPath.CARRIER, headers: {'Authorization': 'bearer $tToken'}));
    });

    test('should return carrier when request is successful', () async {
      when(httpClient.get(any, headers: anyNamed('headers')))
          .thenAnswer((_) async => json.decode(fixture('carrier.json')));
      final tCarrierModel = CarrierModel(
          id: 1, firstName: 'Hadi', lastName: 'Tahmasbi', phone: tPhoneNumber);
      final result = await remoteDataSourceImpl.getProfile(tToken);

      expect(tCarrierModel, equals(result));
    });

    test('should throw s ServerException when there is an error', () async {
      when(httpClient.get(any, headers: anyNamed('headers'))).thenThrow(
          ServerException(statusCode: 403, message: 'not authorized'));
      final call = remoteDataSourceImpl.getProfile;

      expect(() => call(tToken), throwsA(TypeMatcher<ServerException>()));
    });
  });
}
