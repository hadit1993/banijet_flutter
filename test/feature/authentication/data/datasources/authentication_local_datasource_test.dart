import 'dart:convert';

import 'package:banijet_flutter/core/error/exception.dart';
import 'package:banijet_flutter/feature/authentication/data/datasources/authentication_local_datasource.dart';
import 'package:banijet_flutter/feature/authentication/data/models/carrier_model.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:matcher/matcher.dart';
import '../../../../core/mocks/mocks.dart';
import '../../../../fixtures/fixture_reader.dart';

void main() {
  AuthenticationLocalDataSourceImpl localDataSourceImpl;
  MockSharedPreferences mockSharedPreferences;
  final tToken = 'test';

  setUp(() {
    mockSharedPreferences = MockSharedPreferences();
    localDataSourceImpl = AuthenticationLocalDataSourceImpl(
        sharedPreferences: mockSharedPreferences);
  });

  group('test caching token', () {
    test(
        'should return token from shared preferences when there is one in the cache',
        () async {
      when(mockSharedPreferences.getString(any)).thenReturn(tToken);

      final result = await localDataSourceImpl.getAuthenticationToken();

      verify(mockSharedPreferences.getString(AUTH_TOKEN_KEY));

      expect(result, equals(tToken));
    });

    test('should throw a CacheException when there is no cached token', () {
      when(mockSharedPreferences.getString(any)).thenReturn(null);
      final call = localDataSourceImpl.getAuthenticationToken;
      expect(() => call(), throwsA(TypeMatcher<CacheException>()));
    });

    test('should call sharedPreferences to cache token', () async {
      await localDataSourceImpl.saveAuthenticationToken(tToken);

      verify(mockSharedPreferences.setString(AUTH_TOKEN_KEY, tToken));
    });

    test('should call sharedPreferences to delete token', () async {
      await localDataSourceImpl.deleteAuthenticationToken();

      verify(mockSharedPreferences.remove(AUTH_TOKEN_KEY));
    });

    test('should call sharedPreferences to check if the token has exists',
        () async {
      when(mockSharedPreferences.getString(AUTH_TOKEN_KEY)).thenReturn(tToken);

      final result = await localDataSourceImpl.hasToken();

      verify(mockSharedPreferences.getString(AUTH_TOKEN_KEY));

      expect(result, true);
    });
  });

  group('test caching profile', () {
    test(
        'should return carrier from shared preference when there is one in the cache',
        () async {
      final tCarrierModel =
          CarrierModel.fromJson(json.decode(fixture('carrier.json')));

      when(mockSharedPreferences.getString(any))
          .thenReturn(fixture('carrier.json'));

      final result = await localDataSourceImpl.getCachedProfile();

      verify(mockSharedPreferences.getString(CACHED_CARRIER_KEY));

      expect(result, equals(tCarrierModel));
    });

    test('should throw CacheException when there is no profile in the cache',
        () async {
      when(mockSharedPreferences.getString(any)).thenReturn(null);
      final call = localDataSourceImpl.getCachedProfile;
      expect(() => call(), throwsA(TypeMatcher<CacheException>()));
    });

    test('should call sharedPreference  to cache profile', () async {
      final tCarrierModel = CarrierModel(
          id: 1, firstName: 'Hadi', lastName: 'Tahmasbi', phone: '09361958846');
      await localDataSourceImpl.cacheProfile(tCarrierModel);
      final expectedJsonString = json.encode(tCarrierModel.toJson());
      verify(mockSharedPreferences.setString(
          CACHED_CARRIER_KEY, expectedJsonString));
    });
  });
}
