import 'package:banijet_flutter/core/constants/colors.dart';
import 'package:banijet_flutter/core/constants/text_resources.dart';
import 'package:banijet_flutter/core/cubit/input_validation_cubit.dart';
import 'package:banijet_flutter/core/cubit/request_cubit.dart';
import 'package:banijet_flutter/feature/authentication/presentation/cubit/request_auth_cubit.dart';
import 'package:banijet_flutter/feature/authentication/presentation/pages/request_auth_page.dart';
import 'package:banijet_flutter/feature/authentication/presentation/widgets/button.dart';
import 'package:banijet_flutter/feature/authentication/presentation/widgets/phone_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../../../core/mocks/mocks.dart';

void main() {
  MockInputValidationCubit inputValidationCubit;
  MockRequestAuthCubit requestAuthCubit;

  setUp(() {
    inputValidationCubit = MockInputValidationCubit();
    requestAuthCubit = MockRequestAuthCubit();
  });

  group('testing request_auth_page', () {
    testWidgets('check the appearance of initial screen state', (tester) async {
      when(inputValidationCubit.state)
          .thenAnswer((_) => InvalidInput(input: ''));
      when(requestAuthCubit.state).thenAnswer((_) => RequestInitial());

      await tester.pumpWidget(MaterialApp(
        home: MultiBlocProvider(
          providers: [
            BlocProvider<InputValidationCubit>.value(
                value: inputValidationCubit),
            BlocProvider<RequestAuthCubit>.value(value: requestAuthCubit),
          ],
          child: RequestAuthPage(),
        ),
      ));

      final PhoneInput phoneInput =
          tester.widget(find.byKey(Key('phoneInput')));
      final Button continueButton = tester.widget(find.byKey(Key('continue')));

      expect(phoneInput.hasError, false);
      expect(phoneInput.errorMessage, '');
      expect(phoneInput.isDeleteIconVisible, false);
      expect(phoneInput.isInputValid, false);
      expect(continueButton.disabled, true);
      expect(continueButton.isLoading, false);
      expect(continueButton.title, TextResources.CONTINUE);
    });

    testWidgets('test when the input is invalid', (tester) async {
      when(inputValidationCubit.state)
          .thenAnswer((_) => InvalidInput(input: '936189'));
      when(requestAuthCubit.state).thenAnswer((_) => RequestInitial());

      await tester.pumpWidget(MaterialApp(
        home: MultiBlocProvider(
          providers: [
            BlocProvider<InputValidationCubit>.value(
                value: inputValidationCubit),
            BlocProvider<RequestAuthCubit>.value(value: requestAuthCubit),
          ],
          child: RequestAuthPage(),
        ),
      ));

      final PhoneInput phoneInput =
      tester.widget(find.byKey(Key('phoneInput')));
      final Button continueButton = tester.widget(find.byKey(Key('continue')));

      expect(phoneInput.hasError, false);
      expect(phoneInput.errorMessage, '');
      expect(phoneInput.isDeleteIconVisible, true);
      expect(phoneInput.isInputValid, false);
      expect(phoneInput.enabled, true);
      expect(continueButton.disabled, true);
      expect(continueButton.isLoading, false);
      expect(continueButton.title, TextResources.CONTINUE);
    });

    testWidgets('test when the input is valid', (tester) async {
      when(inputValidationCubit.state)
          .thenAnswer((_) => ValidInput(input: '09361958846'));
      when(requestAuthCubit.state).thenAnswer((_) => RequestInitial());

      await tester.pumpWidget(MaterialApp(
        home: MultiBlocProvider(
          providers: [
            BlocProvider<InputValidationCubit>.value(
                value: inputValidationCubit),
            BlocProvider<RequestAuthCubit>.value(value: requestAuthCubit),
          ],
          child: RequestAuthPage(),
        ),
      ));

      final PhoneInput phoneInput =
      tester.widget(find.byKey(Key('phoneInput')));
      final Button continueButton = tester.widget(find.byKey(Key('continue')));

      expect(phoneInput.hasError, false);
      expect(phoneInput.errorMessage, '');
      expect(phoneInput.isDeleteIconVisible, true);
      expect(phoneInput.isInputValid, true);
      expect(continueButton.disabled, false);
      expect(continueButton.isLoading, false);
      expect(continueButton.title, TextResources.CONTINUE);
    });

    testWidgets('test when there is loading', (tester) async {
      when(inputValidationCubit.state)
          .thenAnswer((_) => ValidInput(input: '09361958846'));
      when(requestAuthCubit.state).thenAnswer((_) => RequestLoading());

      await tester.pumpWidget(MaterialApp(
        home: MultiBlocProvider(
          providers: [
            BlocProvider<InputValidationCubit>.value(
                value: inputValidationCubit),
            BlocProvider<RequestAuthCubit>.value(value: requestAuthCubit),
          ],
          child: RequestAuthPage(),
        ),
      ));

      final PhoneInput phoneInput =
      tester.widget(find.byKey(Key('phoneInput')));
      final Button continueButton = tester.widget(find.byKey(Key('continue')));

      expect(phoneInput.hasError, false);
      expect(phoneInput.errorMessage, '');
      expect(phoneInput.isDeleteIconVisible, true);
      expect(phoneInput.isInputValid, true);
      expect(phoneInput.enabled, false);
      expect(continueButton.disabled, false);
      expect(continueButton.isLoading, true);
      expect(continueButton.title, TextResources.CONTINUE);
    });

    testWidgets('test when there is error', (tester) async {
      when(inputValidationCubit.state)
          .thenAnswer((_) => ValidInput(input: '9361958846'));
      when(requestAuthCubit.state).thenAnswer((_) => RequestError(message: 'error'));

      await tester.pumpWidget(MaterialApp(
        home: MultiBlocProvider(
          providers: [
            BlocProvider<InputValidationCubit>.value(
                value: inputValidationCubit),
            BlocProvider<RequestAuthCubit>.value(value: requestAuthCubit),
          ],
          child: RequestAuthPage(),
        ),
      ));

      final PhoneInput phoneInput =
      tester.widget(find.byKey(Key('phoneInput')));
      final Button continueButton = tester.widget(find.byKey(Key('continue')));

      expect(phoneInput.hasError, true);
      expect(phoneInput.errorMessage, 'error');
      expect(phoneInput.isDeleteIconVisible, true);
      expect(phoneInput.isInputValid, true);
      expect(continueButton.disabled, false);
      expect(continueButton.isLoading, false);
      expect(continueButton.title, TextResources.CONTINUE);
    });
  });
}
