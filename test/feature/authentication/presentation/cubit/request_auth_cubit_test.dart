import 'package:banijet_flutter/core/cubit/request_cubit.dart';

import 'package:banijet_flutter/feature/authentication/presentation/cubit/request_auth_cubit.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:banijet_flutter/feature/authentication/domain/usecases/request_authentication.dart';
import 'package:banijet_flutter/core/error/failures.dart';

import '../../../../core/mocks/mocks.dart';

void main() {
  RequestAuthCubit cubit;
  MockRequestAuthentication requestAuthentication;

  setUp(() {
    requestAuthentication = MockRequestAuthentication();

    cubit = RequestAuthCubit(requestAuthentication: requestAuthentication);
  });

  test('initial state should be RequestInitial', () {
    expect(cubit.state, equals(RequestInitial()));
  });

  group('test request auth', () {
    final tParam = AuthRequestParam(phoneNumber: '09361958846');

    final tExpiration = 120;

   

    blocTest(
        'should emit [RequestLoading, RequestSuccess] when request is successful',
        build: () {
          when(requestAuthentication(any))
              .thenAnswer((_) async => Right(tExpiration));
          return cubit;
        },
        act: (c) => c(tParam),
        expect: [
          RequestLoading(),
          RequestSuccess(result: tExpiration)
        ],
       verify: (_) {
         verify(requestAuthentication(
             tParam)).called(1);
       }

    );

    final error = 'not found';
    blocTest(
        'should emit [RequestLoading, RequestError] when request is failed',
        build: () {
          when(requestAuthentication(any))
              .thenAnswer((_) async => Left(ServerFailure(404, error)));
          return cubit;
        },
        act: (c) => c(tParam),
        expect: [RequestLoading(), RequestError(message: error)]);
  });
}
