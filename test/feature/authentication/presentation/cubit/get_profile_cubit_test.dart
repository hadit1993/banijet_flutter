import 'package:banijet_flutter/core/cubit/request_cubit.dart';
import 'package:banijet_flutter/core/utils/usecase.dart';
import 'package:banijet_flutter/feature/authentication/data/models/carrier_model.dart';
import 'package:banijet_flutter/feature/authentication/domain/entities/carrier.dart';

import 'package:banijet_flutter/feature/authentication/domain/usecases/verify_authentication.dart';
import 'package:banijet_flutter/feature/authentication/presentation/cubit/get_profile_cubit.dart';

import 'package:banijet_flutter/feature/authentication/presentation/cubit/verify_auth_cubit.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import 'package:banijet_flutter/core/error/failures.dart';

import '../../../../core/mocks/mocks.dart';

void main() {
  GetProfileCubit cubit;
  MockGetProfile getProfile;

  setUp(() {
    getProfile = MockGetProfile();

    cubit = GetProfileCubit(getProfile: getProfile);
  });

  test('initial state should be RequestInitial', () {
    expect(cubit.state, equals(RequestInitial()));
  });

  group('test get profile', () {
    final tCarrier = Carrier(
        id: 1, firstName: 'Hadi', lastName: 'Tahmasbi', phone: '09361958846');

    blocTest(
        'should emit [RequestLoading, RequestSuccess] when request is successful',
        build: () {
          when(getProfile(any))
              .thenAnswer((_) async => Right(tCarrier));
          return cubit;
        },
        act: (c) => c(NoParams()),
        expect: [RequestLoading(), RequestSuccess(result: tCarrier)],
        verify: (_) {
          verify(getProfile(NoParams())).called(1);
        });

    final error = 'not found';
    blocTest(
        'should emit [RequestLoading, RequestError] when request is failed',
        build: () {
          when(getProfile(any))
              .thenAnswer((_) async => Left(ServerFailure(404, error)));
          return cubit;
        },
        act: (c) => c(NoParams()),
        expect: [RequestLoading(), RequestError(message: error)]);
  });
}
