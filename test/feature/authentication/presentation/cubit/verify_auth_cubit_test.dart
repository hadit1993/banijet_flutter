import 'package:banijet_flutter/core/cubit/request_cubit.dart';
import 'package:banijet_flutter/feature/authentication/domain/usecases/verify_authentication.dart';

import 'package:banijet_flutter/feature/authentication/presentation/cubit/verify_auth_cubit.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import 'package:banijet_flutter/core/error/failures.dart';

import '../../../../core/mocks/mocks.dart';

void main() {
  VerifyAuthCubit cubit;
  MockVerifyAuthentication verifyAuthentication;

  setUp(() {
    verifyAuthentication = MockVerifyAuthentication();

    cubit = VerifyAuthCubit(verifyAuthentication: verifyAuthentication);
  });

  test('initial state should be RequestInitial', () {
    expect(cubit.state, equals(RequestInitial()));
  });

  group('test verify auth', () {
    final tParam =
        AuthVerifyParams(phoneNumber: '09361958846', verificationCode: '12345');

    final String tToken = 'test';

    blocTest(
        'should emit [RequestLoading, RequestSuccess] when request is successful',
        build: () {
          when(verifyAuthentication(any))
              .thenAnswer((_) async => Right(tToken));
          return cubit;
        },
        act: (c) => c(tParam),
        expect: [RequestLoading(), RequestSuccess(result: tToken)],
        verify: (_) {
          verify(verifyAuthentication(tParam)).called(1);
        });

    final error = 'not found';
    blocTest(
        'should emit [RequestLoading, RequestError] when request is failed',
        build: () {
          when(verifyAuthentication(any))
              .thenAnswer((_) async => Left(ServerFailure(404, error)));
          return cubit;
        },
        act: (c) => c(tParam),
        expect: [RequestLoading(), RequestError(message: error)]);
  });
}
