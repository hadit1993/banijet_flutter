import 'dart:async';

import 'package:banijet_flutter/feature/authentication/presentation/cubit/timer_cubit.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../../../core/mocks/mocks.dart';

void main() {
  TimerCubit cubit;
  MockTicker ticker;

  setUp(() {
    ticker = MockTicker();
    cubit = TimerCubit(ticker: ticker);
  });

  group('test TimerCubit', () {
    final expectedOutput = [5, 4, 3, 2, 1, 0];
    final tTimer = 5;

    blocTest(
        'cubit should emit [TimerActive(5), TimerActive(4), ...] when timer started',
        build: () {
          when(ticker.tick(ticks: anyNamed('ticks')))
              .thenAnswer((_) => Stream.fromIterable(expectedOutput));
          return cubit;
        },
        act: (TimerCubit cubit) => cubit.startTimer(tTimer),
        verify: (_) {
          verify(ticker.tick(ticks: tTimer)).called(1);
        },
        expect: [
          TimerActive(5),
          TimerActive(4),
          TimerActive(3),
          TimerActive(2),
          TimerActive(1),
          TimerInactive()
        ]);

    blocTest('cubit should emit [TimerInactive()] when timer stopped',
        build: () {
          return cubit;
        },
        act: (TimerCubit cubit) => cubit.stopTimer(),
        expect: [TimerInactive()]);
  });
}
