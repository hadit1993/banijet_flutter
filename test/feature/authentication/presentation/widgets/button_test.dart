import 'package:banijet_flutter/core/constants/colors.dart';
import 'package:banijet_flutter/core/cubit/input_validation_cubit.dart';
import 'package:banijet_flutter/feature/authentication/presentation/widgets/button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../../../core/mocks/mocks.dart';

void main() {
  MockInputValidationCubit inputValidationCubit;

  setUp(() {
    inputValidationCubit = MockInputValidationCubit();
  });
  group('testing Button', () {
    final buttonColor = Colors.blue;
    final titleColor = Colors.red;
    final splashColor = Colors.grey;
    final disabledColor = Colors.amber;

    testWidgets('test initial state widget', (tester) async {
      await tester.pumpWidget(BlocProvider<InputValidationCubit>.value(
          value: inputValidationCubit,
          child: BlocBuilder<InputValidationCubit, InputValidationState>(
              builder: (context, state) {
            return MaterialApp(
              home: Button(
                title: 'test',
                onPress: () {
                  inputValidationCubit.validate('test');
                },
              ),
            );
          })));

      await tester.tap(find.byKey(Key('inkWell')));
      verify(inputValidationCubit.validate('test'));

      expect(find.text('test'), findsOneWidget);
      expect(find.byKey(Key('buttonLoading')), findsNothing);
      expect(tester.widget<Material>(find.byKey(Key('material'))).color,
          BanijetColors.PRIMARY);
      expect(tester.widget<Text>(find.text('test')).style.color, Colors.white);
    });

    testWidgets('test button colors', (tester) async {
      await tester.pumpWidget(MaterialApp(
        home: Button(
          title: 'test',
          onPress: null,
          color: buttonColor,
          titleColor: titleColor,
          splashColor: splashColor,
        ),
      ));

      expect(tester.widget<Material>(find.byKey(Key('material'))).color,
          buttonColor);
      expect(tester.widget<Text>(find.text('test')).style.color, titleColor);
      expect(tester.widget<InkWell>(find.byKey(Key('inkWell'))).splashColor,
          splashColor);
    });

    testWidgets('test disabled state', (tester) async {
      await tester.pumpWidget(BlocProvider<InputValidationCubit>.value(
          value: inputValidationCubit,
          child: BlocBuilder<InputValidationCubit, InputValidationState>(
              builder: (context, state) {
            return MaterialApp(
              home: Button(
                title: 'test',
                onPress: () {
                  inputValidationCubit.validate('test');
                },
                disabled: true,
                disabledColor: disabledColor,
              ),
            );
          })));

      await tester.tap(find.byKey(Key('inkWell')));
      verifyNever(inputValidationCubit.validate('test'));
      expect(find.text('test'), findsOneWidget);
      expect(find.byKey(Key('buttonLoading')), findsNothing);
      expect(tester.widget<Material>(find.byKey(Key('material'))).color,
          disabledColor);
    });
    testWidgets('test loading state', (tester) async {
      await tester.pumpWidget(BlocProvider<InputValidationCubit>.value(
          value: inputValidationCubit,
          child: BlocBuilder<InputValidationCubit, InputValidationState>(
              builder: (context, state) {
            return MaterialApp(
              home: Button(
                title: 'test',
                onPress: () {
                  inputValidationCubit.validate('test');
                },
                isLoading: true,
              ),
            );
          })));

      await tester.tap(find.byKey(Key('inkWell')));
      verifyNever(inputValidationCubit.validate('test'));
      expect(find.text('test'), findsNothing);
      expect(find.byKey(Key('buttonLoading')), findsOneWidget);
    });
  });
}
