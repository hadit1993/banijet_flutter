import 'package:banijet_flutter/core/constants/colors.dart';
import 'package:banijet_flutter/core/cubit/input_validation_cubit.dart';
import 'package:banijet_flutter/feature/authentication/presentation/widgets/phone_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../../../core/mocks/mocks.dart';

void main() {
  TextEditingController controller;

  setUp(() {});

  group('testing the phone input', () {
    testWidgets('when input is empty', (tester) async {
      controller = TextEditingController();
      await tester.pumpWidget(
        MaterialApp(
            home: Scaffold(
          body: Container(
            child: PhoneInput(
                onDelete: null,
                isDeleteIconVisible: false,
                phoneController: controller,
                hasError: false,
                onChanged: null,
                enabled: true,
                isInputValid: false,
                errorMessage: null),
          ),
        )),
      );

      expect(tester.widget<Image>(find.byKey(Key('inputProfileIcon'))).color,
          BanijetColors.DISABLED_COLOR);
      expect(find.byKey(Key('delIcon')), findsNothing);
      expect(find.byKey(Key('errorText')), findsNothing);
      expect(
          tester.widget<TextField>(find.byKey(Key('phoneTextField'))).enabled,
          true);
    });

    testWidgets('when input is invalid', (tester) async {
      controller = TextEditingController(text: '123456');
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: Container(
                child: PhoneInput(
                    onDelete: () {
                      controller.clear();
                    },
                    isDeleteIconVisible: true,
                    phoneController: controller,
                    hasError: false,
                    onChanged: null,
                    enabled: true,
                    isInputValid: false,
                    errorMessage: null)),
          ),
        ),
      );

      await (tester.tap(find.byKey(Key('delIcon'))));
      expect(controller.text, '');

      expect(tester.widget<Image>(find.byKey(Key('inputProfileIcon'))).color,
          BanijetColors.DISABLED_COLOR);
      expect(find.byKey(Key('delIcon')), findsOneWidget);
      expect(find.byKey(Key('errorText')), findsNothing);
    });

    testWidgets('when input is valid', (tester) async {
      controller = TextEditingController(text: '09361958846');
      await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: Container(
            child: PhoneInput(
                onDelete: null,
                isDeleteIconVisible: false,
                phoneController: controller,
                hasError: false,
                onChanged: null,
                enabled: true,
                isInputValid: true,
                errorMessage: null),
          ),
        ),
      ));

      expect(tester.widget<Image>(find.byKey(Key('inputProfileIcon'))).color,
          null);

      expect(find.byKey(Key('errorText')), findsNothing);
    });

    testWidgets('when there is error', (tester) async {
      controller = TextEditingController(text: '09361958846');
      await tester.pumpWidget(MaterialApp(
        home: Scaffold(
          body: Container(
            child: PhoneInput(
                onDelete: null,
                isDeleteIconVisible: false,
                phoneController: controller,
                hasError: true,
                onChanged: null,
                enabled: true,
                isInputValid: true,
                errorMessage: 'error'),
          ),
        ),
      ));

      expect(find.text('error'), findsOneWidget);
    });

    testWidgets('when onChange method', (tester) async {
      controller = TextEditingController();
      final cubit = MockInputValidationCubit();
      await tester.pumpWidget(MaterialApp(
          home: Scaffold(
        body: BlocProvider<InputValidationCubit>.value(
          value: cubit,
          child: Container(
            child: PhoneInput(
                onDelete: null,
                isDeleteIconVisible: false,
                phoneController: controller,
                hasError: true,
                onChanged: (text) {
                  cubit.validate(text);
                },
                enabled: true,
                isInputValid: true,
                errorMessage: 'error'),
          ),
        ),
      )));

      await tester.enterText(find.byKey(Key('phoneTextField')), '12345');
      verify(cubit.validate('12345'));
    });
  });
}
