


import 'package:banijet_flutter/core/utils/usecase.dart';
import 'package:banijet_flutter/feature/authentication/domain/usecases/is_authenticated.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../../../core/mocks/mocks.dart';

void main() {
  IsAuthenticated isAuthenticated;
  MockAuthenticationRepository repository;

  setUp(() {
    repository = MockAuthenticationRepository();
    isAuthenticated = IsAuthenticated(repository);
  });

  test('should sign out from account', () async {
    when(repository.isAuthenticated()).thenAnswer((_) async => Right(true));

    final result = await isAuthenticated(NoParams());

    expect(result, Right(true));

    verify(repository.isAuthenticated());

    verifyNoMoreInteractions(repository);
  });
}