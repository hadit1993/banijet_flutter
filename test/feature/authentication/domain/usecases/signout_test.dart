import 'package:banijet_flutter/core/utils/usecase.dart';
import 'package:banijet_flutter/feature/authentication/domain/usecases/signout.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../../../core/mocks/mocks.dart';

void main() {
  SignOut signOut;
  MockAuthenticationRepository repository;

  setUp(() {
    repository = MockAuthenticationRepository();
    signOut = SignOut(repository);
  });

  test('should sign out from account', () async {
    when(repository.signOut()).thenAnswer((_) async => Right(true));

    final result = await signOut(NoParams());

    expect(result, Right(true));

    verify(repository.signOut());

    verifyNoMoreInteractions(repository);
  });
}
