import 'package:banijet_flutter/feature/authentication/domain/usecases/request_authentication.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import '../../../../core/mocks/mocks.dart';

void main() {
  RequestAuthentication requestAuthentication;
  MockAuthenticationRepository repository;

  setUp(() {
    repository = MockAuthenticationRepository();
    requestAuthentication = RequestAuthentication(repository);
  });

  final tPhoneNumber = "09361958846";
  final tResult = 120;

  test('should request authentication with phone number', () async {
    when(repository.requestAuthentication(any))
        .thenAnswer((_) async => Right(tResult));

    final result = await requestAuthentication(
        AuthRequestParam(phoneNumber: tPhoneNumber));

    expect(result, Right(tResult));

    verify(repository.requestAuthentication(tPhoneNumber));

    verifyNoMoreInteractions(repository);
  });
}
