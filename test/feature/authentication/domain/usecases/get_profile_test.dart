




import 'package:banijet_flutter/core/utils/usecase.dart';
import 'package:banijet_flutter/feature/authentication/domain/entities/carrier.dart';
import 'package:banijet_flutter/feature/authentication/domain/usecases/get_profile.dart';
import 'package:banijet_flutter/feature/authentication/domain/usecases/verify_authentication.dart';
import 'package:dartz/dartz.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import '../../../../core/mocks/mocks.dart';

void main() {
  GetProfile getProfile;
  MockAuthenticationRepository repository;

  setUp(() {
    repository = MockAuthenticationRepository();
    getProfile = GetProfile(repository);
  });

  final tProfile = Carrier( id: 1 ,firstName: 'Hadi', lastName: 'Tahmasbi', phone: '09361958846');

  test('should fetch carrier info', () async {
    when(repository.getProfile())
        .thenAnswer((_) async => Right(tProfile));

    final result = await getProfile(NoParams());

    expect(result, Right(tProfile));

    verify(repository.getProfile());

    verifyNoMoreInteractions(repository);
  });
}