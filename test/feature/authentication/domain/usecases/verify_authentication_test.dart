


import 'package:banijet_flutter/feature/authentication/domain/usecases/verify_authentication.dart';
import 'package:dartz/dartz.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import '../../../../core/mocks/mocks.dart';

void main() {
  VerifyAuthentication verifyAuthentication;
  MockAuthenticationRepository repository;

  setUp(() {
    repository = MockAuthenticationRepository();
    verifyAuthentication = VerifyAuthentication(repository);
  });

  final tPhoneNumber = "09361958846";
  final tVerificationCode = '12345';
  final tToken = 'test';

  test('should verify authentication with phone number and verification code', () async {
    when(repository.verifyAuthentication(any, any))
        .thenAnswer((_) async => Right(tToken));

    final result = await verifyAuthentication(
        AuthVerifyParams(phoneNumber: tPhoneNumber, verificationCode: tVerificationCode));

    expect(result, Right(tToken));

    verify(repository.verifyAuthentication(tPhoneNumber, tVerificationCode));

    verifyNoMoreInteractions(repository);
  });
}